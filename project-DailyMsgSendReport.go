package main

import (
	"bytes"
	"crypto/tls"
	"database/sql"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"github.com/cavaliercoder/grab"
	_ "github.com/go-sql-driver/mysql"
	"github.com/printfhome/goutils"
	"html/template"
	"io/ioutil"
	"log"
	"net/smtp"
	"os"
	"strconv"
	"strings"
	"time"
)

var (
	db                              *sql.DB
	err                             error
	fetchCounterFromCSV             []string
	counterForMonth                 = []int{}
	counterForAccounts              = []int{}
	containCounterForMonth          = []int{}
	incrementCounter                = 0
	usernameSlice                   = []string{}
	activeUserSlice                 = []int{}
	totalOfNoOfMsgSendToday         int
	totalOfNoOfMsgInMonth           int
	totalOfNoOfActiveUser           int
	totalOfNoOfEmailSentToday       int
	totalOfNoOfEmailSentInMonth     int
	totalOfNoOfCallProceededToday   int
	totalOfNoOfCallProceededInMonth int
	forAppend                       []string
	toConvertIntToString            = []string{}
	monthCounter                    = [][]string{}
	lowerboundarySlice              []int
	upperboundarySlice              []int

	config  []ForConfigFile
	mailIds []To
	obj     SliceOfStructs

	configForHTMLTemplate ForHTMLTemplateConfigFile

	emailCounterSlice    = []int{}
	emailCounterForMonth = []int{}
	callCounterSlice     = []int{}
	callCounterForMonth  = []int{}
	//to get current date
	current_time = time.Now().Add(-24 * time.Hour).Local()

	emailIdSlice []string
)

//keys of JsonFile (for AccountInfo)
type ForConfigFile struct {
	Accountusername string
	Dbname          string
	Dbuser          string
	Dbpass          string
	Dbtablename     string
	Upperboundary   int
	Lowerboundary   int
	ForActiveUser   string
}

//
type To struct {
	Email_Id string
}

type ForHTMLTemplateConfigFile struct {
	DisplaySMSCounter   string
	DisplayEmailCounter string
	DisplayCallCounter  string
}

type ForTotalRow struct {
	TotalString               string
	TotalMsgSendToday         int
	TotalMsgInMonth           int
	TotalActiveUser           int
	TotalEmailSentToday       int
	TotalEmailSentInMonth     int
	TotalCallProceededToday   int
	TotalCallProceededInMonth int
}

type Mail struct {
	Sender  string
	To      []string
	Cc      []string
	Bcc     []string
	Subject string
	Body    []byte
}

type SliceOfStructs struct {
	Accounts   []AccountInfo
	Total      ForTotalRow
	Permission ForHTMLTemplateConfigFile
}

type AccountInfo struct {
	Name                     string
	MsgSendToday             int
	MsgInMonth               int
	ActiveUser               int
	Lowerboundary            int
	Upperboundary            int
	NoOfEmailSentToday       int
	NoOfEmailSentInMonth     int
	NoOfCallPreceededToday   int
	NoOfCallPreceededInMonth int
}

type SmtpServer struct {
	Host      string
	Port      string
	TlsConfig *tls.Config
}

func (s *SmtpServer) ServerName() string {
	return s.Host + ":" + s.Port
}

func (mail *Mail) BuildMessage() string {
	header := ""
	header += fmt.Sprintf("From: %s\r\n", mail.Sender)
	if len(mail.To) > 0 {
		header += fmt.Sprintf("To: %s\r\n", strings.Join(mail.To, ";"))
	}
	if len(mail.Cc) > 0 {
		header += fmt.Sprintf("Cc: %s\r\n", strings.Join(mail.Cc, ";"))
	}

	header += fmt.Sprintf("Subject: %s\r\n", mail.Subject)

	return header
}

func (mail *Mail) BuildMessage2() []byte {
	var header2 []byte
	header2 = mail.Body
	return header2

}

func downloadExcelFileFromTheAPI() {

	//to get current date
	current_time := time.Now().Add(-24 * time.Hour).Local()

	// create client
	client := grab.NewClient()
	var dynamicURL string
	dynamicURL = "http://ip.shreesms.net/smsserver/xmlReport.aspx?username=zydusw&password=12345&from=" + current_time.Format("2006-01-02") + "%2000:00:01&to=" + current_time.Format("2006-01-02") + "%2023:59:59&Type=csv"

	req, _ := grab.NewRequest(".", dynamicURL)

	resp := client.Do(req)
	// check for errors
	if err := resp.Err(); err != nil {
		fmt.Fprintf(os.Stderr, "Download failed: %v\n", err)
		os.Exit(1)
	}
}

func retriveDataFromTheJsonFile() {

	//retrive data from the config.json
	filePath := "./config.json"
	fmt.Printf("// reading file %s\n", filePath)
	file, err1 := ioutil.ReadFile(filePath)
	if err1 != nil {
		fmt.Printf("// error while reading file %s\n", filePath)
		fmt.Printf("File error: %v\n", err1)
		os.Exit(1)
	}

	var objmap map[string]*json.RawMessage
	err2 := json.Unmarshal(file, &objmap)
	if err2 != nil {
		fmt.Println("error:", err2)
		os.Exit(1)
	}

	// fmt.Println("config is", objmap)

	err = json.Unmarshal(*objmap["Account"], &config)
	// fmt.Println("config is", config)

	err = json.Unmarshal(*objmap["GrantPermission"], &configForHTMLTemplate)
	// fmt.Println("configForHTMLTemplate is", configForHTMLTemplate)

	err = json.Unmarshal(*objmap["Receiver"], &mailIds)
	// fmt.Println("mailIds ==", mailIds)
}

func getTheNoOfMsgSentToday(name string) {
	//slice which contains all the Account's username
	usernameSlice = append(usernameSlice, name)

	csvfile, err := os.Open("Report.csv")

	if err != nil {
		fmt.Println(err)
		return
	}
	defer csvfile.Close()
	r := csv.NewReader(csvfile)
	records, err := r.ReadAll()

	//now here if the CSV file cell's is mathched with the account username then counter will increment and it will give us NO. Of Msg Send Today to the perticular Account..which we are gonna place in counterForAccount Slice
	for _, record := range records {
		for _, cell := range record {
			if cell == name {
				incrementCounter++
			}
		}
	}
	if err != nil {
		log.Fatal(err)
	}
	counterForAccounts = append(counterForAccounts, incrementCounter)
	incrementCounter = 0
}

func fetchTheMonthCounterFromExcelFile() {

	csvfile, err := os.Open("MonthCounterForAllAccount.csv")

	if err != nil {
		fmt.Println(err)
		return
	}
	defer csvfile.Close()
	r := csv.NewReader(csvfile)
	//here records will contain Account Month Counter Along With Account UserName
	records, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	//retrive all the account counter from the CSV file and store it in counterForMonth we are gonna add current counterForAccounts in it later
	for _, record := range records {
		fetchCounterFromCSV = append(fetchCounterFromCSV, record[1])
	}

	//records has rows of excel file in string format but we need in Int Format coz this roows are actually counters of all account
	for _, i := range fetchCounterFromCSV {
		intForm, err := strconv.Atoi(i)
		if err != nil {
			panic(err)
		}
		counterForMonth = append(counterForMonth, intForm)
	}

	//index of both slice must be same coz whenever new client comes counterForAccount has it's today's Counter but still it's monthCounter is nill while retriving from the excel file..so bydefault we are gonna assign 0..
	if len(counterForAccounts) != len(counterForMonth) {
		counterForMonth = append(counterForMonth, 0)
		fmt.Println("this will happen only one time whenever new client join")
	}
}

func createExcelFileWhichContainMonthCounter() {

	specialTime := time.Now().Add(-24 * time.Hour)

	//Create CSV File to Store all Account Counter For Month
	csvfile, err := os.Create("MonthCounterForAllAccount.csv")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	defer csvfile.Close()

	// //now increment the _______ForMonth with the current date counter
	for index, _ := range counterForAccounts {
		modifiedMonthCounter := counterForMonth[index] + counterForAccounts[index]
		containCounterForMonth = append(containCounterForMonth, modifiedMonthCounter)
	}

	//when month's last date comes
	fmt.Println("here current_time =", specialTime.Format("2006-01-02"))
	fmt.Println("here TimeBeginningOfMonth  =", goutils.TimeBeginningOfMonth(specialTime).Format("2006-01-02"))
	if specialTime.Format("2006-01-02") == goutils.TimeBeginningOfMonth(specialTime).Format("2006-01-02") {
		for index, _ := range counterForMonth {
			containCounterForMonth[index] = counterForAccounts[index]
		}
	}
	// fmt.Println("containcounterfor month is ", containCounterForMonth)

	//same to write in CSV file we need counter in string formate not int.
	for _, i := range containCounterForMonth {
		strForm := strconv.Itoa(i)
		toConvertIntToString = append(toConvertIntToString, strForm)
	}

	//to write desired formate([][]string{{"item1", "value1"}, {"item2", "value2"}, {"item3", "value3"}}) in CSV FILE
	for index, _ := range usernameSlice {
		forAppend = []string{usernameSlice[index], toConvertIntToString[index]}
		monthCounter = append(monthCounter, forAppend)
	}
	writer := csv.NewWriter(csvfile)
	for _, record := range monthCounter {
		err := writer.Write(record)
		if err != nil {
			fmt.Println("Error:", err)
			return
		}
	}
	writer.Flush()
}

func retriveActiveUserOfAllAccountFromTheDatabase(databaseName string, username string, password string, tableName string, magicString string) {

	var activeUserCounter, monthEmailCounter, emailCounter, callCounter, monthCallCounter int

	//for how many email sent in month
	t := time.Now().Add(-24 * time.Hour).Local()
	forSpecificMonth := t.Format("%2006-01-%")
	fmt.Println(forSpecificMonth)

	// Create an sql.DB and check for errors
	var serverinfo string
	serverinfo = "root:" + password + "@/" + databaseName + "?parseTime=true"
	db, err = sql.Open("mysql", serverinfo)

	if err != nil {
		panic(err.Error())
	}
	// sql.DB sho	uld be long lived "defer" closes it once this function ends
	defer db.Close()

	// Test the connection to the database
	err = db.Ping()
	if err != nil {
		panic(err.Error())
	}
	//to get active users
	var query string
	query = "SELECT COUNT(id) FROM " + tableName + " WHERE status = ?"

	err = db.QueryRow(query, magicString).Scan(&activeUserCounter)
	if err != nil {
		log.Fatal(err)
	}
	//toGetNoOfEmailSendToday
	toGetNoOfEmailSendToday := fmt.Sprintf("SELECT COUNT(id) FROM campaign_sent_records WHERE sending_date like '%s'", current_time.Format("2006-01-02%"))
	err = db.QueryRow(toGetNoOfEmailSendToday).Scan(&emailCounter)
	if err != nil {
		log.Fatal(err)
	}
	//toGetNoOfEmailSentInMonth
	// query1 = "SELECT COUNT(id) FROM campaign_sent_records WHERE month(sending_date) = '04' and year(sending_date)= '2017'"
	toGetNoOfEmailSentInMonth := fmt.Sprintf("SELECT COUNT(id) FROM campaign_sent_records WHERE sending_date like '%s'", forSpecificMonth)
	err = db.QueryRow(toGetNoOfEmailSentInMonth).Scan(&monthEmailCounter)
	if err != nil {
		log.Fatal(err)
	}
	//toGetNoOfCallPreceeded
	toGetNoOfCallPreceeded := fmt.Sprintf("SELECT COUNT(id) FROM android_callsms_log WHERE time like '%s' AND type = 'SMS'", current_time.Format("2006-01-02%"))
	err = db.QueryRow(toGetNoOfCallPreceeded).Scan(&callCounter)
	if err != nil {
		log.Fatal(err)
	}
	//toGetNoOfCallPreceededInMonth
	toGetNoOfCallPreceededInMonth := fmt.Sprintf("SELECT COUNT(id) FROM android_callsms_log WHERE time like '%s' AND type = 'SMS'", forSpecificMonth)
	err = db.QueryRow(toGetNoOfCallPreceededInMonth).Scan(&monthCallCounter)
	if err != nil {
		log.Fatal(err)
	}
	//activeUserSlice will contain the No. of Active user of all account..
	activeUserSlice = append(activeUserSlice, activeUserCounter)
	//emailCounterSlice will contain the No. of email sent today for all the accounts..
	emailCounterSlice = append(emailCounterSlice, emailCounter)
	//emailCounterForMonth will contain the No. of email sent in month for all the accounts..
	emailCounterForMonth = append(emailCounterForMonth, monthEmailCounter)
	//callCounterSlice will contain the No. of call proceeded today for all the accounts..
	callCounterSlice = append(callCounterSlice, callCounter)
	//callCounterForMonth will contain the No. of call proceeded in month for all the accounts..
	callCounterForMonth = append(callCounterForMonth, monthCallCounter)
}

func calculateTheTotalOfEveryColumnInHTMLTemplateAndCreateJSONBasedObject() {

	for index, _ := range counterForAccounts {
		totalOfNoOfMsgSendToday = totalOfNoOfMsgSendToday + counterForAccounts[index]
		totalOfNoOfMsgInMonth = totalOfNoOfMsgInMonth + containCounterForMonth[index]
		totalOfNoOfActiveUser = totalOfNoOfActiveUser + activeUserSlice[index]
		totalOfNoOfEmailSentToday = totalOfNoOfEmailSentToday + emailCounterSlice[index]
		totalOfNoOfEmailSentInMonth = totalOfNoOfEmailSentInMonth + emailCounterForMonth[index]
		totalOfNoOfCallProceededToday = totalOfNoOfCallProceededToday + callCounterSlice[index]
		totalOfNoOfCallProceededInMonth = totalOfNoOfCallProceededInMonth + callCounterForMonth[index]
	}

	//this will be the first coloum of TotalRow in HTMLTemplate
	totalString := "Total"

	//SliceOfStructs Object
	for index, _ := range usernameSlice {
		obj.Accounts = append(obj.Accounts, AccountInfo{Name: usernameSlice[index], MsgSendToday: counterForAccounts[index], MsgInMonth: containCounterForMonth[index], ActiveUser: activeUserSlice[index], Lowerboundary: lowerboundarySlice[index], Upperboundary: upperboundarySlice[index], NoOfEmailSentToday: emailCounterSlice[index], NoOfEmailSentInMonth: emailCounterForMonth[index], NoOfCallPreceededToday: callCounterSlice[index], NoOfCallPreceededInMonth: callCounterForMonth[index]})
	}
	obj.Total = ForTotalRow{TotalString: totalString, TotalMsgSendToday: totalOfNoOfMsgSendToday, TotalMsgInMonth: totalOfNoOfMsgInMonth, TotalActiveUser: totalOfNoOfActiveUser, TotalEmailSentToday: totalOfNoOfEmailSentToday, TotalEmailSentInMonth: totalOfNoOfEmailSentInMonth, TotalCallProceededToday: totalOfNoOfCallProceededToday, TotalCallProceededInMonth: totalOfNoOfCallProceededInMonth}

	obj.Permission = ForHTMLTemplateConfigFile{DisplaySMSCounter: configForHTMLTemplate.DisplaySMSCounter, DisplayEmailCounter: configForHTMLTemplate.DisplayEmailCounter, DisplayCallCounter: configForHTMLTemplate.DisplayCallCounter}

	//get the email_ids from the json and append it in emailIdSlice
	for index := range mailIds {
		emailIdSlice = append(emailIdSlice, mailIds[index].Email_Id)
	}
	// fmt.Println("emailIdSlice ==", emailIdSlice)
}

func sendDailyMSGSentReportAlongWithHTMLTemplate() {

	//now For sending Mails
	mail := Mail{}
	mail.Sender = "support@mybms.in"
	//mail.To = []string{ /*"govind@savitriya.com", "amit@savitriya.com", "mrugen@savitriya.com", "keyur@mybms.in", "keyur@savitriya.com", */ "pr@savitriya.com"}
	mail.To = emailIdSlice
	mail.Cc = []string{}
	mail.Bcc = []string{ /*"keyur@mybms.in"*/ }
	mime := "MIME-Version: 1.0" + "\r\n" +
		"Content-type: text/html" + "\r\n"
	mail.Subject = fmt.Sprintf("Daily Message Sent Report For %s", current_time.Format("02-01-2006"))
	//template

	// Create a template using template.html
	tmpl, err := template.New("HTMLTemplate").ParseFiles("./header.html", "body.html", "footer.html")
	if err != nil {
		log.Printf("Error: %s", err)
		return
	}

	// Stores the parsed template
	var buff bytes.Buffer

	params := map[string]interface{}{"AccountsInformation": obj.Accounts, "Total": obj.Total, "Permission": obj.Permission}
	fmt.Println("param is", params)

	// Send the parsed template to buff
	err = tmpl.Execute(&buff, params)
	if err != nil {
		log.Printf("Error: %s", err)
	}

	//for mail body...
	mail.Body = []byte(mime + buff.String())

	messageBody := mail.BuildMessage()
	var messageBody2 []byte
	messageBody2 = mail.BuildMessage2()

	smtpServer := SmtpServer{Host: "smtp.gmail.com", Port: "465"}
	smtpServer.TlsConfig = &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpServer.Host,
	}

	auth := smtp.PlainAuth("", mail.Sender, "mybms123", smtpServer.Host)

	conn, err := tls.Dial("tcp", smtpServer.ServerName(), smtpServer.TlsConfig)
	if err != nil {
		log.Panic(err)
	}

	client, err := smtp.NewClient(conn, smtpServer.Host)
	if err != nil {
		log.Panic(err)
	}

	// step 1: Use Auth
	if err = client.Auth(auth); err != nil {
		log.Panic(err)
	}

	// step 2: add all from and to
	if err = client.Mail(mail.Sender); err != nil {
		log.Panic(err)
	}
	receivers := append(mail.To, mail.Cc...)
	receivers = append(receivers, mail.Bcc...)
	for _, k := range receivers {
		log.Println("sending to: ", k)
		if err = client.Rcpt(k); err != nil {
			log.Panic(err)
		}
	}

	// Data
	w, err := client.Data()
	if err != nil {
		log.Panic(err)
	}

	_, err = w.Write([]byte(messageBody))
	_, err = w.Write([]byte(messageBody2))
	if err != nil {
		log.Panic(err)
	}

	err = w.Close()
	if err != nil {
		log.Panic(err)
	}

	client.Quit()
}

func deleteDownloadedCSVFile() {
	err := os.Remove("Report.csv")
	if err != nil {
		log.Fatal(err)
	}
}

func main() {

	downloadExcelFileFromTheAPI()
	retriveDataFromTheJsonFile()

	for k := range config {
		getTheNoOfMsgSentToday(config[k].Accountusername)
	}

	fetchTheMonthCounterFromExcelFile()
	createExcelFileWhichContainMonthCounter()

	for t := range config {
		retriveActiveUserOfAllAccountFromTheDatabase(config[t].Dbname, config[t].Dbuser, config[t].Dbpass, config[t].Dbtablename, config[t].ForActiveUser)
		upperboundarySlice = append(upperboundarySlice, config[t].Upperboundary)
		lowerboundarySlice = append(lowerboundarySlice, config[t].Lowerboundary)
	}

	calculateTheTotalOfEveryColumnInHTMLTemplateAndCreateJSONBasedObject()

	sendDailyMSGSentReportAlongWithHTMLTemplate()
	deleteDownloadedCSVFile()
}
